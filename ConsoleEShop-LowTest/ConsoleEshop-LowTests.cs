using System;
using Xunit;
using ConsoleEShop_Low;
using System.Collections.Generic;
using System.Collections;

namespace ConsoleEShop_LowTest
{
    public class ConsoleEShop_LowTest
    {
        [Fact]
        public void ProductDBAddingProductAndFindReturnCorrectValue()
        {
            //Arrange
            ProductDB productDB = new ProductDB();
            var expected = new Product("First", 100, "First", new Category("First"));

            //Act
            productDB.Add(expected);
            var actual = productDB.Find(expected.ProductName);

            //Assert
            Assert.Equal(expected.ProductName, actual.ProductName);
        }

        [Fact]
        public void OrderDBdingOrderAndFindReturnCorrectValue()
        {
            //Arrange
            OrderDB orderDB = new OrderDB();
            var product = new Product("First", 100, "First", new Category("First"));
            var expected = new Order(new Address("Ukraine", "Kiev", "102"), 1, new OrderItem(product, 2));

            //Act
            orderDB.Add(expected);
            var actual = orderDB.Find(expected.ID);

            //Assert
            Assert.Equal(expected.ID, actual.ID);
        }

        [Fact]
        public void UserDBAddingUserAndFindReturnCorrectValue()
        {
            //Arrange
            UserDB userDB = new UserDB();
            var expected = new RegisteredUser("login", "password");

            //Act
            userDB.Add(expected);
            var actual = userDB.Find(expected);

            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void OrderDBAddingOrderReturnFalse()
        {
            //Arrange
            OrderDB orderDB = new OrderDB();
            var product = new Product("First", 100, "First", new Category("First"));
            var order = new Order(new Address("Ukraine", "Kiev", "102"), 1, new OrderItem(product, 2));
            var orderCopy = new Order(new Address("Ukraine", "Kiev", "102"), 1, new OrderItem(product, 2));
            //Act
            orderDB.Add(order);

            //Assert
            Assert.False(orderDB.Add(orderCopy), "You can't add order with same ID");
        }
        [Fact]
        public void OrderDBAddingThrowException()
        {
            //Arrange
            OrderDB orderDB = new OrderDB();
            Order p = null;

            //Assert
            Assert.Throws<ArgumentNullException>(() => orderDB.Add(p));
        }


        [Fact]
        public void UserDBAddingReturnFalse()
        {
            //Arrange
            UserDB userDB = new UserDB();
            var user = new RegisteredUser("login", "password");
            var userCopy = new RegisteredUser("login", "password");
            //Act
            userDB.Add(user);

            //Assert
            Assert.False(userDB.Add(userCopy), "You can't add user with same login, password");
        }

        [Fact]
        public void UserDBAddingThrowException()
        {
            //Arrange
            UserDB userDB = new UserDB();
            User u = null;

            //Assert
            Assert.Throws<ArgumentNullException>(() => userDB.Add(u));
        }

        [Fact]
        public void ProductDBAddingReturnFalse()
        {
            ProductDB productDB = new ProductDB();
            var product = new Product("First", 100, "First", new Category("First"));
            var productCopy = new Product("First", 100, "First", new Category("First"));

            //Act
            productDB.Add(product);

            //Assert
            Assert.False(productDB.Add(productCopy), "Product already exist");
        }



        [Fact]
        public void ProductDBAddingThrowException()
        {
            //Arrange
            ProductDB productDB = new ProductDB();
            Product p = null;

            //Assert
            Assert.Throws<ArgumentNullException>(() => productDB.Add(p));
        }

        [Theory]
        [MemberData(nameof(TestDataGenerator.GetProductFromDataGenerator), MemberType = typeof(TestDataGenerator))]
        public void ProductDBAddingReturnTrue(params Product[] data)
        {
            //Arrange
            ProductDB productDB = new ProductDB();

            //Assert
            foreach (var i in data)
            {
                Assert.True(productDB.Add(i));
            }
        }

        [Theory]
        [MemberData(nameof(TestDataGenerator.GetUsersFromDataGenerator), MemberType = typeof(TestDataGenerator))]
        public void UserDBAddingReturnTrue(params User[] data)
        {
            //Arrange
            UserDB userDB = new UserDB();

            //Assert
            foreach (var i in data)
            {
                Assert.True(userDB.Add(i));
            }
        }

        [Theory]
        [MemberData(nameof(TestDataGenerator.GetUsersFromDataGenerator), MemberType = typeof(TestDataGenerator))]
        public void UserIDGeneratesCorrectly(params User[] data)
        {
            //Assert
            var expected = data[0].Id;
            foreach (var member in data)
            {
                Assert.Equal(expected++, member.Id);
            }
        }


        [Theory]
        [MemberData(nameof(TestDataGenerator.GetProductFromDataGenerator), MemberType = typeof(TestDataGenerator))]
        public void ProductIDGeneratesCorrectly(params Product[] data)
        {
            //Assert
            int expected = data[0].Id;
            foreach (var member in data)
            {
                Assert.Equal(expected++, member.Id);
            }
        }


        [Theory]
        [MemberData(nameof(TestDataGenerator.GetOrdersFromDataGenerator), MemberType = typeof(TestDataGenerator))]
        public void OrderIDGeneratesCorrectly(params Order[] data)
        {
            //Assert
            int expected = data[0].ID;
            foreach (var member in data)
            {
                Assert.Equal(expected++, member.ID);
            }
        }

        [Fact]
        public void ProdutEqualsReturnFalse()
        {
            //Arrange
            Product product = new Product("First", 100, "First", new Category("First"));

            var produt1 = new Product("First", 99, "First", new Category("First"));

            //Assert
            Assert.False(product.Equals(produt1));
        }

        [Fact]
        public void ProdutEqualsReturnTrue()
        {
            //Arrange
            Product product = new Product("First", 100, "First", new Category("First"));

            var produt1 = new Product("First", 100, "First", new Category("First"));

            //Assert
            Assert.True(product.Equals(produt1));
        }
        [Fact]
        public void OrderEqualsReturnFalse()
        {
            //Arrange
            var order = new Order(new Address("Ukraine", "Kiev", "102"), 1, new OrderItem(new Product("First", 100, "Second", new Category("First")), 2));
            var orderCopy = new Order(new Address("Ukraine", "Kiev", "102"), 1, new OrderItem(new Product("First", 100, "Second", new Category("First")), 2));

            //Assert
            Assert.True(order.Equals(orderCopy));
        }

        [Fact]
        public void CreatingOfRegisteredUserReturnsRegistersUserPermission()
        {
            var user = new RegisteredUser();
            var expected = UserPermission.RegisteredUser;
            //Assert
            Assert.Equal(expected, user.Permission);
        }


        [Theory]
        [MemberData(nameof(TestDataGenerator.GetDifferrentUserOrderID), MemberType = typeof(TestDataGenerator))]
        public void UserAddingNewOrderReturnTrue(params int[] id)
        {
            var user = new RegisteredUser();
            //Assert
            foreach (var data in id)
            {
                Assert.True(user.AddNewOrder(data));
            }
        }

 


        [Fact]
        public void CreatingOfAdminReturnsAdminPermission()
        {
            var admin = new Admin();
            var expected = UserPermission.Admin;
            //Assert
            Assert.Equal(expected, admin.Permission);
        }
        public class TestDataGenerator
        {
            public static IEnumerable<object[]> GetProductFromDataGenerator()
            {
                yield return new object[]
                {
                    new Product("First", 100, "First", new Category("First")),
                    new Product("Second", 10, "Second", new Category("ddf")),
                    new Product("Third", 100, "Third", new Category("Third"))
                };
            }

            public static IEnumerable<object[]> GetUsersFromDataGenerator()
            {
                yield return new object[]
                {
                    new RegisteredUser("user1", "user1"),
                    new RegisteredUser("user2", "user2"),
                    new RegisteredUser("user3", "user3")
                };
            }

            public static IEnumerable<object[]> GetDifferrentUserOrderID()
            {
                yield return new object[]
                {
                   1,2,3,4,5
                };
            }
            public static IEnumerable<object[]> GetEqualsUserOrderID()
            {
                yield return new object[]
                {
                   1,1
                };
            }

            public static IEnumerable<object[]> GetOrdersFromDataGenerator()
            {
                yield return new object[]
                {
                    new Order(new Address("Ukraine", "Kiev", "102"), 1, new OrderItem(new Product("First", 100, "Second", new Category("First")), 2)),
                    new Order(new Address("Ukraine", "VN", "10254"), 1, new OrderItem(new Product("Second", 100, "Second", new Category("First")), 1)),
                    new Order(new Address("Ukraine", "Od", "10255"), 1, new OrderItem(new Product("Third", 100, "Third", new Category("First")), 24)),
                    new Order(new Address("Ukraine", "KH", "10244"), 1, new OrderItem(new Product("Fourth", 100, "Fourth", new Category("First")), 22))
                };
            }
        }
    }
}
