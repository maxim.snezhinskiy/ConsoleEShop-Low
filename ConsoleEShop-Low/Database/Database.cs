﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public static class Database
    {
        public static UserDB Users { get; set; }
        public static OrderDB Orders { get; set; }
        public static ProductDB Products { get; set; } 

        static Database()
        {
        
            Products = new ProductDB(new List<Product>()
            {
                new Product("Iphone 12", 1199, "Apple Iphone",  new Category("Phones")),
                new Product("Samsung Galaxy s41", 999, "Samsung corporation",  new Category("Phones")),
                new Product("Mercedes benz E-200", 68999, "The best car ever",  new Category("Cars")),
                new Product("Apple airPods", 199, "Apple Inc", new Category("HeadPhones"))
            });

            Users = new UserDB(new List<User>()
            {
                new RegisteredUser("simpleUSer", "password"),
                new RegisteredUser("user1", "user1"),
                new Admin("admin", "admin"),
            });

            Orders = new OrderDB();
        }

    }
}
