﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class UserDB : IEnumerable<User>, IDatabase<User, IUser>
    {

        public List<User> Users { get; set; }
        public int Count { get => Users.Count; }


        public UserDB() 
        {
            Users = new List<User>();
        }

        public UserDB(List<User> users) { Users = users; }

        public User this[int index]
        {
            get
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                return Users[index];
            }
            set
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("", new Exception());
                }
                Users[index] = value;
            }
        }

        public bool Add(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            if (UserExist(user.Login, user.Password))
                return false;
            Users.Add(user);
            return true;
        }

        public User Find(IUser user) 
        {
            return Users.Find(i=> i.Login == user.Login && i.Password == user.Password);
        }

        public bool UserExist(string login) 
        {
            return Users.Exists(i=> i.Login == login);
        }

        public bool UserExist(string login, string password)
        {
            return Users.Exists(i => i.Login == login && i.Password == password);
        }
        public IEnumerator<User> GetEnumerator()
        {
            return Users.GetEnumerator() as IEnumerator<User>;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Users.GetEnumerator();
        }
    }
}
