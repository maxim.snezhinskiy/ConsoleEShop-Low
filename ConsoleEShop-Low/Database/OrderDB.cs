﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleEShop_Low
{
    public class OrderDB : IEnumerable<Order>, IDatabase<Order, int>
    {
        public List<Order> Orders { get; set; }

        public int Count => Orders.Count;

        public Order this[int index]
        {
            get
            {
                if (index < 0 || index > Orders.Count)
                {
                    throw new ArgumentException($"index is > {Orders.Count}");
                }
                return Orders[index];
            }
            set
            {
                if (index < 0 || index > Orders.Count)
                {
                    throw new ArgumentException($"index is > {Orders.Count}");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("", new Exception());
                }
                Orders[index] = value;
            }
        }
        public OrderDB()
        {
            Orders = new List<Order>();
        }
        public OrderDB(IEnumerable<Order> orders)
        {
            Orders = orders.ToList();
        }
        public OrderDB(List<Order> orders) : this()
        {
            Orders = orders;
        }



        public Order Find(int id) 
        {
            return Orders.Find(i=> i.ID == id);
        }

        public bool Add(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            if (Orders.Contains(order))
                return false;

            Orders.Add(order);
            return true;
        }

        public IEnumerator<Order> GetEnumerator()
        {
            return ((IEnumerable<Order>)this).GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return Orders.GetEnumerator();
        }
    }
}
