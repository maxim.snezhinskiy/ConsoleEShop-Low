﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class ProductDB : IEnumerable<Product>, IDatabase<Product, string>
    {
        public List<Product> Products { get; private set; }
        public int Count { get => Products.Count; }

        public Product this[int index]
        {
            get
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                return Products[index];
            }
            set
            {
                if (index < 0 || index > Count)
                {
                    throw new ArgumentException($"index is > {Count}");
                }
                if (value == null)
                {
                    throw new ArgumentNullException("", new Exception());
                }
                Products[index] = value;
            }
        }

        public ProductDB()
        {
            Products = new List<Product>();
        }
        public ProductDB(IEnumerable<Product> products)
        {
            Products = new List<Product>(products);
        }
        public ProductDB(List<Product> products)
        {
            Products = products;
        }

        public bool Add(Product product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(this.Add), new Exception());

            if (Products.Contains(product))
                return false;

            Products.Add(product);
            return true;
        }

        public Product Find(string name) 
        {
            if (name == null)
            {
                throw new ArgumentNullException("", new Exception());
            }
            return Products.Find(i=> i.ProductName.ToLower() == name.ToLower());
        }
        public List<Product> GetDBase()
        {
            return Products;
        }

        public override string ToString()
        {
            string res = "";
            foreach (var i in Products) 
            {
                res += i.ToString();
            }
            return res;
        }

        public IEnumerator GetEnumerator()
        {
            return Products.GetEnumerator();
        }
        IEnumerator<Product> IEnumerable<Product>.GetEnumerator()
        {
            return ((IEnumerable<Product>)Products).GetEnumerator();
        }
    }
}
