﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public interface IAuthorization
    {
        User SignUp(string login, string password);
        bool SignIn(string login, string password);
    }
}
