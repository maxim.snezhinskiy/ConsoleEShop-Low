﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{

    public abstract class ClientMenu : IMenu
    {

        public EventHandler LoginNotify { get; set; }
        public EventHandler LogOutNotify { get ; set; }

        public Product FindProduct(string name)
        {
            return Database.Products.Find(name);
        }

        public abstract void PrintMenu();
        public abstract void Execute();
        public abstract void GetUserInput(string data);


        public void SearchProduct()
        {
            Console.Clear();
            Console.Write("\nEnter product name: ");
            var entered = Console.ReadLine();
            var product = Database.Products.Find(entered);
            if (product != null)
            {
                Console.WriteLine("\n"+ product);
            }
            else
            {
                Console.WriteLine("No product found!");
            }
            Console.WriteLine("\nPress any key go back to menu...");
            Console.ReadLine();
        }
        public virtual void PrintData<T>(IEnumerable<T> data)
        {
            Console.Clear();
            int indx=1;
            foreach (var i in data)
            {
                Console.WriteLine($"{indx++}. {i.ToString()}\n");
                
            }
            Console.WriteLine();
        }

        public virtual void PrintPoductList() 
        {
            Console.WriteLine("--- Products ---");
            PrintData(GetAllProducts());
        }
        public List<Product> GetAllProducts()
        {
            return Database.Products.GetDBase();
        }

    
    }
}
