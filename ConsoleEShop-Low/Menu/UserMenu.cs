﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class UserMenu<T> : ClientMenu
           where T : RegisteredUser
    {

        public virtual T user { get; set; }
        public UserMenu(T account) { user = account; }

        public override void PrintMenu()
        {
            Console.WriteLine($"You loggined as {user.Login}");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("-- Menu");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("1. Show all products");
            Console.WriteLine("2. Search product by name");
            Console.WriteLine("3. Create new order");
            Console.WriteLine("4. Order history and delivery status");
            Console.WriteLine("5. Change personal info");
            Console.WriteLine("6. Change order status");
            Console.WriteLine("7. Log out");
        }

        public override void GetUserInput(string data)
        {
            int parsed = 0;
            int.TryParse(data, out parsed);
            switch (parsed)
            {
                case 1: PrintPoductList(); break;
                case 2: SearchProduct(); break;
                case 3: ShowAndCreate(); break;
                case 4: PrintOrders(); Console.ReadKey(); break;
                case 5: ChangePersonalInfo(); break;
                case 6: ChangeOrderStatus(); break;
                case 7: LogOut(); break;
                default: break;
            }
        }

        public override void Execute()
        {
            PrintMenu();
            Console.Write("Enter: ");
            GetUserInput(Console.ReadLine());
        }

        public void ChangeOrderStatus()
        {
            do
            {
                var hasOrders = PrintOrders();
                if (!hasOrders)
                    return;
                Console.WriteLine("\n1 - Set status \"Received\"");
                Console.WriteLine("2 - Set status \"Canceled\"");
                Console.WriteLine("3 - Back to menu");
                int choice; Order order;
                Console.Write("Enter: ");
                choice = int.Parse(Console.ReadLine());
                var success = false;
                switch (choice)
                {
                    case 1:
                        Console.Write("-- Enter order number to set status: ");
                        choice = int.Parse(Console.ReadLine());
                        order = Database.Orders[choice - 1];
                        success = user.SetStatusReceived(order); break;
                    case 2:
                        Console.Write("-- Enter order number to set status: ");
                        choice = int.Parse(Console.ReadLine());
                        order = Database.Orders[choice - 1];
                        success = user.SetStatusCanceled(order); break;
                    case 3: return;
                    default: break;
                }
                if (!success)
                {
                    Console.WriteLine("You can't set this status to order!");
                }
                else
                {
                    Console.WriteLine("Status changed successfully!");
                }
                Console.ReadKey();
            } while (true);
        }

        public void ChangePersonalInfo()
        {
            EditUserInfo(user);
        }

        public void EditUserInfo(User account)
        {
            do
            {
                Console.Clear();
                Console.WriteLine("\t\t\t\t-- Personal information --");
                Console.WriteLine(user);

                Console.WriteLine("\n--What to change");
                Console.WriteLine("1 - Name ");
                Console.WriteLine("2 - Email ");
                Console.WriteLine("3 - Phone number ");
                Console.WriteLine("4 - Back");
                Console.Write("Enter: ");
                int choice = int.Parse(Console.ReadLine()); // try
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter new Name: ");
                        user.Name = Console.ReadLine();
                        break;
                    case 2:
                        Console.Write("Enter new Email: ");
                        user.contact.Email = Console.ReadLine();
                        break;
                    case 3:
                        Console.Write("Enter new Phone number: ");
                        user.contact.PhoneNumber = Console.ReadLine();
                        break;
                    case 4: return;
                    default: break;
                }
            } while (true);
        }

        public bool PrintOrders()
        {
            var orders = GetUserOrders();
            Console.Clear();
            if (orders == null || orders.Count == 0)
            {
                Console.WriteLine("You have no orders!");
                Console.ReadKey();
                return false;
            }
            Console.WriteLine("\t\t\t\t\t-- Your orders --");
            foreach (var i in orders)
            {
                Console.WriteLine(i);
                Console.WriteLine();
            }
            return true;
        }

        private List<Order> GetUserOrders()
        {
            var ordersId = user.GetUserOrdersId();
            List<Order> orders = new List<Order>();
            foreach (var i in ordersId)
            {
                orders.Add(Database.Orders.Find(i));
            }
            return orders;
        }

        public void LogOut()
        {
            LogOutNotify?.Invoke(user, new EventArgs());
        }

        public void ShowAndCreate()
        {
            base.PrintPoductList();
            Console.Write("Do you want to make an order?(y/n- go back to menu): ");
            string input = Console.ReadLine();
            if (input == "y")
            {
                CreateNewOrder();
            }
        }



        public void CreateNewOrder()
        {
            int productNum;
            do
            {
                Console.Write("-- Enter product num to create order: ");
                productNum = int.Parse(Console.ReadLine());
            } while (productNum < 0 || productNum > Database.Products.Count);
            Console.Clear();

            var product = GetProduct(productNum);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(product);
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("-- Enter count of products: ");
            int count = int.Parse(Console.ReadLine());
            var items = new OrderItem(product, count);

            Console.WriteLine("-- Enter address to shipping -- ");
            var address = GetAddress();
            var order = new Order(items, address, user.Id);
            Console.Write("Do you confirm an order?(y/n- go back to menu): ");
            string input = Console.ReadLine();
            if (input == "y")
            {
                user.AddNewOrder(order.ID);
                AddOrderToDB(order);
            }
            else
            {
                return;
            }
            Console.Clear();
            PrintUserOrder(order.ID);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Your order has been successfully formed");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadKey();
        }


        private void AddOrderToDB(Order order)
        {
            Database.Orders.Add(order);
        }

        private void PrintUserOrder(int orderId)
        {
            Order order = Database.Orders.Find(orderId);
            Console.WriteLine(order);
        }

        private Address GetAddress()
        {
            var addres = new Address();
            Console.Write("Enter country: ");
            addres.Country = Console.ReadLine();

            Console.Write("Enter city: ");
            addres.City = Console.ReadLine();

            Console.Write("Enter bulding: ");
            addres.Building = Console.ReadLine();

            return addres;
        }

        public override void PrintPoductList()
        {
            base.PrintPoductList();
            Console.WriteLine("Press any key back to menu...");
            Console.ReadKey();
        }
        private Product GetProduct(int indx)
        {
            return Database.Products[indx - 1];
        }

    }
}
