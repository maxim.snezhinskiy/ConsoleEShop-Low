﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Address
    {
        public string Country { get; set; }
        public string City { get; set; }
        public string Building { get; set; }
        public Address(){}
        public Address(string country, string city, string building)
        {
            Country = country;
            City = city;
 
            Building = building;
        }
        public override string ToString() => $"{Country}, {City} city, building {Building}";
    }
}
