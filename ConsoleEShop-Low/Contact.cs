﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Contact
    {
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Contact()
        {

        }

        public Contact(string phoneNumber, string email)
        {
            PhoneNumber = phoneNumber;
            Email = email;
        }
        public override string ToString()
        {
            return $"Phone number: {PhoneNumber ?? "Unknown"}\n" +
                $"Email: {Email ?? "Unknown"}";
        }
    }
}
