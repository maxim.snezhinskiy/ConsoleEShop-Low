﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Category
    {
        public string Name { get; set; }

        public Category()
        {
            
        }

        public Category(string name)
        {
            Name = name;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
