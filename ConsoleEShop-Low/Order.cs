﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Order
    {
        private static int id;
        public int ID { get; } = 0;
        public Address OrderAddress { get; set; }
        public List<OrderItem> Items { get; set; }
        public OrderStatus OrderStatus { get;  set; }
        public bool isConfirmed { get; set; }

        public int UserID { get; private set; }

        public Order()
        {
            id++;
            ID = id;
            Items = new List<OrderItem>();
            OrderStatus = OrderStatus.New;
            OrderAddress = new Address();
        }

        public Order(OrderItem item, Address address, int userId) : this()
        {

            Items.Add(item);
            OrderAddress = address;
            UserID = userId;
        }

        public Order(Address address, int userId, params OrderItem[] items) : this()
        {
            Items.AddRange(items);
            OrderAddress = address;
            UserID = userId;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Order o)
            {
                return this.ID != o.ID;
            }
            return false;
        }

        public void AddOrderItem(OrderItem item)
        {
            if (item == null)
            {
                throw new ArgumentNullException(nameof(this.AddOrderItem), new Exception());
            }
            Items?.Add(item);
        }
        public List<OrderItem> GetOrderItems() => Items;

        public override string ToString()
        {
            string items = "";
            foreach (var i in Items)
            {
                items +="*"+ i;
            }
            return $"-- Order №{this.ID}--\n" +
                   $"-{items}\n" +
                   $"Order status: {OrderStatus.ToString()}\n"+
                   $"{OrderAddress}";
        }
    }
}
