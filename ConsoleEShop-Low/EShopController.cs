﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public static class EShopController
    {
        public static User currentUser { get; set; }
        public static IMenu menu { get; set; }

        public static void Start() 
        {
            menu = new GuestMenu();
            while (true)
            {
                Console.Clear();

                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\t\t\t\t\t------ ConsoleEShop-Low ------");
                Console.ResetColor();

                menu.LoginNotify += LogIn;
                menu.Execute();
            }
        }

        public static void LogIn(object sender, EventArgs e)
        {
            currentUser = sender as User;
            if (currentUser.Permission == UserPermission.RegisteredUser)
            {
                menu = new UserMenu<RegisteredUser>(currentUser as RegisteredUser);
                menu.LogOutNotify += LogOut;
            }
            if (currentUser.Permission == UserPermission.Admin)
            {
                menu = new AdminMenu(currentUser as Admin);
                menu.LogOutNotify += LogOut;
            }
        }

        public static void LogOut(object sender, EventArgs e)
        {
            menu = new GuestMenu();
        }

    }
}
