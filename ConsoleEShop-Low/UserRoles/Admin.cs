﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Admin : RegisteredUser
    {
        public Admin() : base() 
        {
            Permission = UserPermission.Admin;
        }
        public Admin(string login, string password) : base(login, password)
        {
            Permission = UserPermission.Admin;
        }

    }
}
