﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class User : IUser
    {
        public string Name { get; set; }
        private static int ID = 0;
        public UserPermission Permission { get;  protected set; }

        public Contact contact { get; set; } = new Contact();
        public string Login { get; set; }
        public string Password { get; set; }

        public int Id { get; private set; }

        protected User() 
        {
            ID++;
            Id = ID;
        }
        public User(string login, string password) :this()
        {
            Login = login;
            Password = password;
        }
        public override string ToString()
        {
            return $"User ID: {Id}\n" +
                $"Name: {Name ?? "Unknown"}\n" +
                $"Login: {Login}\n" +
                $"{contact?.ToString()}\n";
        }

    }
}
