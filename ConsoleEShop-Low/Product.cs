﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class Product
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public Category Category { get; set; }
        public string Description { get; set; }
        private static int ID=0;
        public int Id { get; }

        public Product()
        {
            ID++;
            Id = ID;
        }
        public Product(string productName, decimal price, string description, Category category) : this()
        {
            ProductName = productName;
            Price = price;
            Description = description;
            Category = category;
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is Product p)
            {
                return this.ProductName == p.ProductName && this.Price == p.Price && this.Description == p.Description;
            }
            return false;
        }
        

    public override string ToString()
    {
        return $"Product ID: {Id} \nName: {ProductName}, price: {Price}\nDescription: {Description},\nCategory: {Category?.Name} ";
    }
}
}
