﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEShop_Low
{
    public class OrderItem
    {
        public int Quantity { get; set; }
        public Product Product { get; set; }
        public OrderItem()
        {

        }
        public OrderItem(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }

        public override string ToString()
        {
            return $"{Product}\nQuantity: {Quantity}";
        }


    }
}
